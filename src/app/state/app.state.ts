import { HistoryState } from './history/history.model';
import {TodoState} from './todo/todo.model';

export interface AppState {
  todo: TodoState,
  history: HistoryState
}