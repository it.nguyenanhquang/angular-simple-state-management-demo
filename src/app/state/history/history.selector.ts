import { createSelector } from "@ngrx/store";
import { AppState } from "../app.state";
import { HistoryState } from "./history.model";

export const selectHistory = (state: AppState) => state.history;
export const selectLogMessage = createSelector(
  selectHistory,
  (state: HistoryState) => state.messages
)
