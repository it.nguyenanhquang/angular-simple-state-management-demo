import { createAction, props } from "@ngrx/store";

export const AddHistory = createAction(
  '[History] Log history',
  props<{content: string}>()
);
