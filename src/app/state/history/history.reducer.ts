import { createReducer, on } from "@ngrx/store";
import { HistoryState } from "./history.model";
import { AddHistory } from "./history.action";

const initialState: HistoryState = {
  messages: []
};

export const historyReducer = createReducer(
  // Init state
  initialState,

  // Event add new history message
  on(AddHistory, (_state, {content}) => ({
    ..._state,
    messages: [..._state.messages, {message: content}]
  }))
)