export interface History {
  message: string;
}

export interface HistoryState {
  messages: History[];
}