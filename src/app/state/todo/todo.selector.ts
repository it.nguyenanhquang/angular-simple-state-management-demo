import { createSelector } from "@ngrx/store";
import { AppState } from "../app.state";
import { TodoState } from "./todo.model";

export const selectTodo = (state: AppState) => state.todo;
export const selectTodoList = createSelector(
  selectTodo,
  (state: TodoState) => state.todoList
)
