export interface Todo {
  id: number;
  content: string;
}

export interface TodoState {
  todoList: Todo[];
}