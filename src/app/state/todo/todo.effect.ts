import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { TodoActions } from "./todo.action";
import { AddHistory } from "../history/history.action";
import { map } from "rxjs/operators";

@Injectable()
export class TodoEffects {

  constructor(private actions$: Actions){}

  addTodo$ = createEffect(() => 
    this.actions$.pipe(
      ofType(TodoActions.addTodo),
      map(action => {
        console.log('Create side effect from add new todo:', action.content);
        return AddHistory({content: 'Add new todo'});
      })
    )
  );

  removeTodo$ = createEffect(() => this.actions$.pipe(
    ofType(TodoActions.removeTodo),
    map(action => {
      console.log('Create side effect from remove todo:', action.id);
      return AddHistory({content: 'Remove todo'});
    })
  ))
}