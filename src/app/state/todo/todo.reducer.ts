import { createReducer, on } from "@ngrx/store";
import { TodoState } from "./todo.model";
import { TodoActions } from "./todo.action";

const initialState: TodoState = {
  todoList: []
}

export const todoReducer = createReducer(
  // Init state
  initialState,

  // Event add new todo
  on(TodoActions.addTodo, (_state, payload) => ({
    ..._state,
    todoList: [..._state.todoList, payload]
  })),

  // Event for remove todo
  on(TodoActions.removeTodo, (_state, {id}) => ({
    ..._state,
    todoList: _state.todoList.filter(item => item.id != id)
  }))
);