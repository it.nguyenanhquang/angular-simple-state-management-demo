import { createActionGroup, props } from "@ngrx/store";
import { Todo } from "./todo.model";

export const TodoActions = createActionGroup({
  source: 'Todo',
  events: {
    'Add Todo': props<Todo>(),
    'Remove Todo': props<{id: number}>()
  }
});