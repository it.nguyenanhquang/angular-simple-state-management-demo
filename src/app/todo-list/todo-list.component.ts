import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectTodoList } from '../state/todo/todo.selector';
import { TodoActions } from '../state/todo/todo.action';
import { AppState } from '../state/app.state';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  public todos$ = this.store.select(selectTodoList);

  constructor(private store: Store<AppState>) {}

  public ngOnInit(): void {}

  public addTodo(content: string) {
    this.store.dispatch(TodoActions.addTodo({
      id: new Date().getTime(),
      content
    }));
  }

  public onDelete(id: number) {
    this.store.dispatch(TodoActions.removeTodo({id}));
  }
}
