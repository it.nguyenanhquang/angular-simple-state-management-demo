import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectLogMessage } from '../state/history/history.selector';
import { AppState } from '../state/app.state';

@Component({
  selector: 'app-action-history',
  templateUrl: './action-history.component.html',
  styleUrls: ['./action-history.component.scss']
})
export class ActionHistoryComponent implements OnInit {
  public logs$ = this.store.select(selectLogMessage);
  constructor(private store: Store<AppState>) {}

  public ngOnInit(): void {}
}
