# Angular Ngrx Store

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.0.

## Demo content
1. Define model
2. Define action
3. Define reducer
4. Define effect
5. Define selector

## ngRx libraries usage:
- [@ngrx/store](https://ngrx.io/guide/store)
- [@ngrx/effects](https://ngrx.io/guide/effects)
- [@ngrx/store-devtools](https://ngrx.io/guide/store-devtools)
